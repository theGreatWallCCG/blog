const path = require('path');
const { config } = require('process');

config.cluster = {
    https:{
        key: path.join(__dirname,'./3_gentlecc.cn.key'), // https 证书绝对目录
        cert: path.join(__dirname,'./gentlecc.cn.csr') 
    }
}